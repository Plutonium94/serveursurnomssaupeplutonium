﻿Assma BENAICHA
Yoann SCHWARTZ
Audric CHABERT 
Daniel Arasu ANBARASU




Projet communication client-serveur : serveur de surnoms


Sommaire :
1. Cahier des charges 
2. Fonctionnement au niveau communication
3. Services proposés
4. Syntaxe des requêtes
5. Gestion des données




1. Cahier des charges


        Pouvoir faire communiquer deux programmes, un couple client-serveur. Les deux programmes communiqueront par l’intermédiaire du protocole de niveau 4 TCP. La syntaxe des requêtes et le format des données seront spécifiées. L'application travaille au niveau de la couche OSI n°7, la couche application. L’application cliente pourra enregistrer, modifier ou récupérer des alias du serveur, le serveur quant à lui stockera et gérera les alias en mémoire tout en traitant les demandes client.


2. Fonctionnement au niveau communication


        La communication se déroulera de la façon suivante : une fois le serveur correctement initialisé, un ou plusieurs clients pourront converser avec lui. Les informations de la requête seront définis en classes, qui seront sérialisées pour la transmission. Au vu de la simplicité du protocole, le serveur traitera chaque requête l’une après l’autre, un client à la fois.
        Le marshalling mis en place est la sérialisation objet. Les informations échangées seront contenues dans des classes qui seront sérialisés puis désérialisés au cours de l’échange. La communication entre le client et le serveur utilisera le port de numéro 1234.




3. Services proposés


Le client doit pouvoir : 
        - ajouter des alias dans la base d’alias du serveur, mais uniquement s’ils respectent les conditions d’admission
        - modifier des alias déjà dans la base d’alias du serveur
        - supprimer des enregistrements d’alias


Le serveur doit pouvoir : 
        - renvoyer à un client une liste d’alias
                - spécifiques à un nom
                - spécifique à un surnom
                - contenant tous les alias stockés par le serveur


4. Gestion des données 


        La correspondance entre un surnom et son nom sera contenue dans une classe Alias. Un ou plusieurs alias, contenus dans une classe message, pourront être échangés par l’application. Une classe message regroupera tous ce que les applications vont échanger. Cette classe aura deux classes filles : une classe requête et une classe réponse. Le client ne fait qu’envoyer des requêtes et recevoir des réponses, et inversement du coté client.


        Voici ci-dessous la liste des classes qui feront serviront aux échanges client-serveur
        
Alias: 


                        - NOM ( String )
                        - SURNOM ( String )




Request : 
                        - CODE ( énumération ) du type de requête :
                                 - ADD_ALIAS
                                 - REMOVE_ALIAS
                                 - GET_ALIASES_FROM_NAME
                                 - GET_NAME_FROM_NICKNAME
                                 - LIST_ALL_NAMES_AND_SURNAMES
                        - ALIAS ( de type Alias )






Response : 
                        - CODE (énumération) du type de réponse :
                                 - ADD_ALIAS
                                 - REMOVE_ALIAS
                                 - GET_ALIASES_FROM_NAME
                                 - GET_NAME_FROM_ALIAS
                                - LIST_ALL_NAMES_AND_SURNAMES
 


                        - CODE d’erreur ( énumération ) :
                                - OK
                                - KO


                        - LISTE D’ALIAS