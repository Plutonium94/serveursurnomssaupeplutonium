package axelProtocole.client;

import java.io.*;
import java.net.Socket;

import axelProtocole.shared.Erreur;

import static java.lang.System.out;


public class Client {

	private Socket clientSocket;
	private PrintWriter os;
	private BufferedReader is;
	public static final int PORT = 6666;

	public static final String DANIEL_MACHINE_NAME = "YXXX.polytech.unice.fr";

	public static final String ADD = "add";
	public static final String EDIT = "edit";
	public static final String GET_NICKNAME = "get_nickname";
	public static final String GET_NAME = "get_name";
	public static final String DELETE_NAME = "del_name";
	public static final String DELETE_NICKNAME = "del_nickname";
 


	public Client() {
		this(DANIEL_MACHINE_NAME);
	}

	public Client(String serveurNomAddresse) {
		try {
			clientSocket = new Socket(serveurNomAddresse, PORT);
			os = new PrintWriter(clientSocket.getOutputStream(),true);
			is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out.println("Client socket defined");
		} catch(IOException ioe) {
			out.println("Client initialisation erreur");
			ioe.printStackTrace();
		}
	}

	public String constructRequest(String nomDuRequest, String...args) {
		String res = nomDuRequest+"#";
		for(String s : args) {
			res += (s + "#");
		}
		res +="\n";
		return res;
	}
	
	public void sendRequest(String req) {
		os.write(req);
	}
	
	public String receiveResponse() {
		String res = null;
		try {
			res = is.readLine();
		} catch(IOException ioe) {
			out.println("Erreur reception réponse du serveur");
		}
		return res;
	}
	
	public void processResponse(String resp) {
		String[] listeInfo = resp.trim().split("#");
		out.println(getError(listeInfo[0]));
		for(int i=1; i< listeInfo.length; i++) {
			out.println(listeInfo[i]);
		}
	}
	
	String getError(String code) {
		return Erreur.valueOf(code).name();
	}



}