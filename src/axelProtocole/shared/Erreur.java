package axelProtocole.shared;

public enum Erreur {
	
	OPERATION_TERMINATED(0),
	COMMAND_NOT_FOUND(1),
	NICKNAME_ALREADY_EXISTS(2),
	NICKNAME_NOT_FOUND(3),
	NAME_NOT_FOUND(4);
	
	private final int code;
	
	private Erreur(int n) {
		code = n;
	}
	
	public int getCode() {
		return code;
	}
	
	
}