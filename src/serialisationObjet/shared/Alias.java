package serialisationObjet.shared;

import java.io.Serializable;

public class Alias implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private String surnom;
	
	public Alias(){
		nom=null;
		surnom=null;
	}
	public Alias(String nom){
		this.nom=nom;
		this.surnom=null;
	}
	
	public Alias(String nom,String surnom){
		this.nom=nom;
		this.surnom=surnom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSurnom() {
		return surnom;
	}

	public void setSurnom(String surnom) {
		this.surnom = surnom;
	}
	
	public String toString(){
		return "Name: "+nom+" alias: "+surnom;
	}
}
