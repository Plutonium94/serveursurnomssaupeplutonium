package serialisationObjet.shared;

import java.util.*;


public class Response extends Message{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3L;
	private List<Alias> listAlias;
	private CodeRequest code;
	private CodeResponse codeR;
	
	public Response(){
		super(CodeRequest.LIST_ALL_NAMES_AND_NICKNAMES);
		this.codeR=CodeResponse.OK;
		this.code=CodeRequest.LIST_ALL_NAMES_AND_NICKNAMES;
		this.listAlias= new ArrayList<Alias>();
	}
	public Response(CodeRequest code){
		super(code);
		this.code=code;
		this.listAlias= new ArrayList<Alias>();
		}
	
	public Response(CodeRequest code,List<Alias> listAlias){
		super(code);
		this.code=code;
		this.listAlias=listAlias;
	}

	public List<Alias> getListAlias() {
		return listAlias;
	}

	public void setListAlias(List<Alias> listAlias) {
		this.listAlias = listAlias;
	}

	public CodeRequest getCode() {
		return code;
	}

	public void setCode(CodeRequest code) {
		this.code = code;
	}
	
	public CodeResponse getCodeR() {
		return codeR;
	}
	public void setCodeR(CodeResponse codeR) {
		this.codeR = codeR;
	}
	public String tostring(){
		return "Code ; "+code+"\n";
	}

}
