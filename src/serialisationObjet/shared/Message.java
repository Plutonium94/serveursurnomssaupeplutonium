package serialisationObjet.shared;

import java.io.Serializable;



public abstract class Message implements Serializable{
	 
	 /**
	 * 
	 */
	private static final long serialVersionUID = 0L;
	private CodeRequest code;
	 
	 public Message(CodeRequest codeMessage){
		 code=codeMessage;
	 }

	public CodeRequest getCode() {
		return code;
	}

	public void setCode(CodeRequest code) {
		this.code = code;
	}
	public String tostring(){
		return " Type Request\n";
	}
	 
}
