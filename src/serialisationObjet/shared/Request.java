package serialisationObjet.shared;


public class Request extends Message{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private Alias alias;
	private CodeRequest code;
	
	public Request(CodeRequest code,Alias alias){
		super(code);
		this.code=code;
		this.alias=alias;
	}

	public Alias getAlias() {
		return alias;
	}

	public void setAlias(Alias alias) {
		this.alias = alias;
	}
	public CodeRequest getcode() {
		return code;
	}
	
	public String tostring(){
		return "Code ; "+code+"\n";
	}
	
}
