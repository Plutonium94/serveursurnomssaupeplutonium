package serialisationObjet.serveur;

import java.util.*;
import java.io.*;

import serialisationObjet.shared.Alias;

public class ListeAlias {
	
	private static TreeMap<String,String> listeAlias = new TreeMap<String,String>();
	private static final String saveFileName = "topSecret.ser";
	
	static {
		for(int i=0; i< 5; i++) {
			listeAlias.put("Surnom" + i, "Nom" + (i/2));
		} 
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(saveFileName));
			oos.writeObject(listeAlias);
			oos.close();
		} catch(Exception e) {
			System.out.println("Oooh ha ha ha!");
		}
	}
	
	public static boolean addAlias(Alias a) {
		if(listeAlias.containsKey(a.getNom())) return false;
		listeAlias.put(a.getSurnom(),a.getNom());
		return true;
	}
	
	public static boolean removeAlias(Alias a) {
		if(!listeAlias.containsKey(a.getSurnom())) return false;
		listeAlias.remove(a.getSurnom());
		return true;
	}
	
	public static boolean updateAlias(Alias a) {
		if(!listeAlias.containsKey(a.getSurnom())) return false;
		listeAlias.put(a.getSurnom(), a.getNom());
		return true;
	}
	
	public static List<Alias> getAliasesFromNom(String nom) {
		List<Alias> res = new ArrayList<Alias>();
		for(Map.Entry<String, String> me : listeAlias.entrySet()) {
			if(me.getValue().equals(nom)) res.add(new Alias(me.getValue(),me.getKey()));
		}
		return res;
	}
	
	public static String getNomFromSurnom(String surnom) {
		return listeAlias.get(surnom);
	}
	
	public static List<Alias> listAllNomsSurnoms() {
		List<Alias> res = new ArrayList<Alias>();
		for(Map.Entry<String, String> me : listeAlias.entrySet()) {
			res.add(new Alias(me.getValue(),me.getKey()));
		}
		return res;
	}
	
	public static boolean save() {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(saveFileName));
			oos.writeObject(listeAlias);
			oos.close();
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean start() {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(saveFileName));
			listeAlias = (TreeMap<String,String>)ois.readObject();
			ois.close();
		} catch(ClassNotFoundException  cnfe) {
			cnfe.printStackTrace();
			return false;
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
		return true;
	}
	
	
}
