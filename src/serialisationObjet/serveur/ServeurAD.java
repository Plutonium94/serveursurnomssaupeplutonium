package serialisationObjet.serveur;

import serialisationObjet.shared.*;

import java.util.List;
import java.util.ArrayList;
import java.net.*;
import java.io.*;


public class ServeurAD implements Runnable {
	
	Socket acceptedSocket = null;
	public static final int PORT = 1234;
	
	public ServeurAD(Socket socket) {
		acceptedSocket = socket;
		ListeAlias.start();
		System.out.println(System.getProperty("user.dir"));
	}
	
	@Override
	public void run() {
		try {
			System.out.println("accepted");
			ObjectOutputStream oos = new ObjectOutputStream(acceptedSocket.getOutputStream());
			for(int i=0;i<50;i++) {
				ObjectInputStream ois = new ObjectInputStream(acceptedSocket.getInputStream());
//				Integer got = (Integer)ois.readObject();
//				Integer sent = got * 5;
//				oos.writeObject(sent);
				Request req = (Request)ois.readObject();
				Response resp = execute(req);
				oos.writeObject(resp);
				System.out.println("J'ecris des choses");
				ois.close();
			}
			oos.close();
			acceptedSocket.close();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		} 
	}
	
	public Response execute(Request req) {
		System.out.println(req);
		CodeRequest cReq = req.getcode();
		List<Alias> la = this.getListeResponse(cReq, req.getAlias());
		CodeResponse cRes = null;
		if(la.isEmpty())  cRes = CodeResponse.KO;
		else cRes = CodeResponse.OK;
		Response res = new Response(req.getcode(),la);
		System.out.println(res);
		return res;
	}
	
	private List<Alias> getListeResponse(CodeRequest c, Alias a) {
		List<Alias> res = new ArrayList<Alias>();
		switch(c) {
			case ADD_ALIAS : {
				if(ListeAlias.addAlias(a)) {
					res.add(a); 
				}
				break;
			} case REMOVE_ALIAS : {
				if(ListeAlias.removeAlias(a)) {
					res.add(a); 
				}
				break;
				
			} case GET_ALIASES_FROM_NAME : {
				return ListeAlias.getAliasesFromNom(a.getNom());
			} case GET_NAME_FROM_NICKNAME : {
				String nom = ListeAlias.getNomFromSurnom(a.getSurnom());
				if(nom != null) {
					res.add(new Alias(nom,a.getSurnom()));
				}
			} case LIST_ALL_NAMES_AND_NICKNAMES : {
				return ListeAlias.listAllNomsSurnoms();
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		ServerSocket serveurSocket = null;
		boolean listening = true;
		try {
			serveurSocket = new ServerSocket(ServeurAD.PORT);
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		while(listening) {
			Thread thread = null;
			try {
				 thread = new Thread(new ServeurAD(serveurSocket.accept()));
			} catch(IOException ioe) {
				System.err.println("Error in serveurSocket.accept()");
				ioe.printStackTrace();
			}
			thread.start();
			try {
				serveurSocket.close();
			} catch(IOException ioe) {
				System.err.println("Error closing serveurSocket");
				ioe.printStackTrace();
			}
		}
	}

}
