package serialisationObjet.client;

import java.io.*;
import java.util.List;
import java.net.Socket;

import serialisationObjet.shared.*;

/*
 * Classe qui implémentera toutes les actions
 * du client.
 */
public class ClientAD 
{
	final static int PORT = 1234;
	final static int TAILLE= 1024;
	public static final String DANIEL_MACHINE_NAME = "YXXX.polytech.unice.fr";
	public static final String DANIEL_MACHINE_ADDRESS = "10.212.116.252";
	
	// Buffer qui contiendra la réponse attendue par le client
	static byte buffer[] = new byte[TAILLE];
	
	public Socket socket = null;
	public Request requete;
	public Response response;
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;
	
	public ClientAD(String serveurNomAddresse) {
		try {
			socket = new Socket(serveurNomAddresse,PORT);
			ois = new ObjectInputStream(socket.getInputStream());
			oos = new ObjectOutputStream(socket.getOutputStream());
			System.out.println("client socket defined");
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public ClientAD() {
		this(DANIEL_MACHINE_ADDRESS);
	}
	
	public void faire() {
		this.defineRequest();
		this.sendRequest();
		this.receiveResponse();
		this.processResponse();
	}
	
	/*
	 * Permettra au client de définir la requête à envoyer
	 * 
	 */
	public Request defineRequest(CodeRequest cr,Alias a)
	{
		requete = new Request(cr,a );
		
		return requete;
	}
	
	public Request defineRequest() {
		return defineRequest(CodeRequest.ADD_ALIAS,new Alias("Yoann","Yoda"));
	}
	
	public boolean sendRequest() {
		return this.sendRequest(this.requete);
	}
	
	public boolean sendRequest(Request requete) {
		try {
			oos.writeObject(requete);
			return true;
		} catch(IOException ioe) {
			System.err.println("Client problem error sending request");
			ioe.printStackTrace();
		}
		return false;
	}
	
	public Response receiveResponse() {
		try {
			response = (Response)ois.readObject();
		} catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} catch(IOException ioe) {
			System.err.println("client error receiving");
			ioe.printStackTrace();
		} 
		return response;
	}
	
	public boolean processResponse() {
		System.out.println("Yea I got a response from the server !");
		List<Alias> la = response.getListAlias();
		if(la != null) System.out.println(la);
		return false;
	}
	
	public void closeAll() {
		try {
			oos.close();
			ois.close();
			socket.close();
		} catch(Exception e) {
			System.err.println("Closing exception");
		}
	}
	
	public static void main(String[] args) {
		ClientAD cad = new ClientAD("172.19.250.220");
		cad.faire();
	}

}
