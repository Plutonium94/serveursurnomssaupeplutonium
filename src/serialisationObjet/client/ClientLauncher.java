package serialisationObjet.client;

import serialisationObjet.shared.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class ClientLauncher {
	
	
	private JFrame frame = new JFrame("Client");
	private JPanel panel = new JPanel();
	private DefaultListModel serveurListModel = new DefaultListModel();
	private JList serveurList;
	private JList codeRequestList;
	private DefaultListModel codeRequestListModel = new DefaultListModel();
	private JLabel nomLabel = new JLabel("Alias nom");
	private JTextField nomField = new JTextField(20);
	private JLabel surnomLabel = new JLabel("Alias surnom");
	private JTextField surnomField = new JTextField(20);
	private JTextArea resultArea = new JTextArea();
	private JButton closeButton = new JButton("Close");
	private JButton sendButton = new JButton("Send");
	
	public ClientLauncher() {
		initLists();
		frameFormalities();
		addToFrame();
		setActionListeners();
	}
	
	private void frameFormalities() {
		frame.setSize(500,500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
	
	private void initLists() {
		for(String s : new String[]{"172.19.250.220",ClientAD.DANIEL_MACHINE_ADDRESS,ClientAD.DANIEL_MACHINE_NAME}) {
			serveurListModel.addElement(s);
		}
		serveurList = new JList(serveurListModel);
		for(Enum e : CodeRequest.values()) {
			codeRequestListModel.addElement(e.name());
		}
		codeRequestList = new JList(codeRequestListModel);
		for(JList jl : new JList[]{serveurList, codeRequestList}) {
			jl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
	}
	
	private void addToFrame() {
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		nomField.setMaximumSize(nomField.getPreferredSize());
		surnomField.setMaximumSize(nomField.getPreferredSize());
		for(JComponent jc : new JComponent[]{serveurList,codeRequestList,nomLabel,nomField,
				surnomLabel, surnomField,resultArea, sendButton,closeButton}) {
			panel.add(jc);
			panel.add(Box.createVerticalStrut(10));
		}
		frame.add(panel);
	}
	
	private void setActionListeners() {
		setSendListener();
		setCloseListener();
	}
	
	void setSendListener() {
		sendButton.addActionListener(new ActionListener() {
			
			private ClientAD client = null;
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				clientInit();
				clientDefineSendRequest();
				showResponse();
			}
			
			private void clientInit() {
				String serveurNomAddresse = (String)serveurList.getSelectedValue();
				if(serveurNomAddresse == null || serveurNomAddresse.trim().equals("")) {
					client = new ClientAD();
				} else {
					client = new ClientAD(serveurNomAddresse);
				}
			}
			
			private void clientDefineSendRequest() {
				Request req = client.defineRequest();;
				String codeRequestString = (String)codeRequestList.getSelectedValue();
				for(Enum e : CodeRequest.values()) {
					if(e.name().equals(codeRequestString)) {
						req = client.defineRequest((CodeRequest)e,new Alias(nomField.getText(),surnomField.getText()));
						break;
					}
				}
				client.sendRequest(req);
			}
			
			private void showResponse() {
				resultArea.setText(client.receiveResponse().toString());
			}
			
		});
	}
		
	private void setCloseListener() {
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				frame.setVisible(false);
			}
		});
	}
	
	public static void main(String[] args) {
		new ClientLauncher();
	}
	
	

}
