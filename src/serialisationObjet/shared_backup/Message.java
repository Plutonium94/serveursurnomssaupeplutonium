package serialisationObjet.shared_backup;

import java.io.*;

/**
 * Un Message comme son nom indique est un message. Ce message est transmis/recu par un
 * serveur/client 
 * @author Plutonium94, Alain Saupe
 *
 */
public abstract class Message implements Serializable {
	
	
	private static final long serialVersionUID = 0L;
	/**
	 * Le code de requete
	 */
	protected final CodeRequest code;
	
	/**
	 * Construit un message avec le code donnee
	 * @param code un code present dans l'enum CodeRequest
	 */
	public Message(CodeRequest code) {
		this.code= code;
	}
	
	/**
	 * Renvoie le code du message
	 * @return
	 */
	public CodeRequest getCodeRequest() {
		return code;
	}
	
	/**
	 * Renvoie une representation textuelle de Message
	 */
	public String toString() {
		return "Message [ codeRequest : " + code + "]"; 
	}

	
	
}
