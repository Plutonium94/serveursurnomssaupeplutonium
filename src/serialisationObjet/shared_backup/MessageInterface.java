package serialisationObjet.shared_backup;

import java.io.Serializable;

public interface MessageInterface extends Serializable {
	
	public abstract CodeRequest getCodeRequest();
}
