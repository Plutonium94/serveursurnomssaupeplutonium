package serialisationObjet.shared_backup;

import java.util.*;

/**
 * Un Response est le Message que le serveur envoie au client.
 * Il contient le code qui est identique au celle du Request qu'il 
 * r�pond, et une liste d'Alias qui est le r�sultat. Il contient aussi un CodeResult
 * qui signifie s'il op�ration est r�ussi ou pas.
 * @author Plutonium94, Alain Saupe
 *
 */
public class Response extends Message {
	
	
	private CodeResponse codeResponse;
	private List<Alias> listeAlias;
	
	public Response() {
		this(CodeRequest.LIST_ALL_NAMES_AND_NICKNAMES,CodeResponse.OK,new ArrayList<Alias>());
	}
	
	/**
	 * Initialize un Response avec le CodeRequest, le CodeResult et 
	 * la liste d'Alias donn�s.
	 * @param code
	 * @param codeResponse
	 * @param listeAlias
	 */
	public Response(CodeRequest code,CodeResponse codeResponse, List<Alias> listeAlias) {
		super(code);
		this.codeResponse = codeResponse;
		this.listeAlias = listeAlias;
	}
	
	/**
	 * Renvoie la liste d'Alias
	 * @return
	 */
	public List<Alias> getListeAlias() {
		return listeAlias;
	}
	
	/**
	 * Renvoie le CodeResult de la Response
	 * @return
	 */
	public CodeResponse getCodeResult() {
		return codeResponse;
	}
	

	/*
	public Response(CodeRequest code) 
	{
	}
	

	public Response(Code code) {
		super(code);
	}
	
	*/
	
	/**
	 * Renvoie une representation textuelle de Response
	 */
	public String toString() {
		return "Response [ codeRequest : " + getCodeRequest() + ", codeResult : "
				+ codeResponse + ", listeAlias : "+ listeAlias + "]";
	}

}
