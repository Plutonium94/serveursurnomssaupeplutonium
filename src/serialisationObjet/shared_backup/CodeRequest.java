package serialisationObjet.shared_backup;

/**
 * Un code qui corresond au requete
 * 
 * ADD_ALIAS : ajouter un alias
 * UPDATE_ALIAS : mettre � jour un alias
 * REMOVE_ALIAS : supprimer un alias
 * GET_ALIASES_FROM_NAME : tous les aliases avec le nom donn�
 * GET_NAME_FROM_NICKNAME : le nom qui correspond � un surnom donn�
 * LIST_ALL_NAMES_AND_NICKNAMES : lister tous les noms et surnoms
 * 
 * @author Plutonium94, Alain Saupe
 *
 */
public enum CodeRequest implements java.io.Serializable {
	
	
	/**
	 * ajouter un alias
	 */
	ADD_ALIAS,
	/**
	 * mettre � jour un alias
	 */
	UPDATE_ALIAS,
	/**
	 * supprimer un alias
	 */
	REMOVE_ALIAS,
	/**
	 * tous les aliases avec le nom donn�
	 */
	GET_ALIASES_FROM_NAME,
	/**
	 * le nom qui correspond � un surnom donn�
	 */
	GET_NAME_FROM_NICKNAME,
	/**
	 * lister tous les noms et surnoms
	 */
	LIST_ALL_NAMES_AND_NICKNAMES

}
