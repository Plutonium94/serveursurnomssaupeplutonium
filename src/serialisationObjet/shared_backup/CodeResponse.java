package serialisationObjet.shared_backup;

/**
 * Un code qui correspond au r�sultat.
 * OK -> succ�s
 * KO -> �chec
 *  
 * @author Plutonium94, Alain Saupe
 *
 */
public enum CodeResponse implements java.io.Serializable{
	
	/**
	 * succ�s : la modification/information souhait� peut �tre r�alis� / existe.
	 */
	OK,
	/**
	 * �chec : la modification/information souhait� ne peut pas �tre fait / n'existe pas
	 */
	KO

}
