package serialisationObjet.shared_backup;

/**
 * Un alias contient un nom et un surnom
 * @author Plutonium94, Alain Saupe
 *
 */
public class Alias implements AliasInterface {
	
	private String nom,surnom;
	
	/**
	 * Construit un alias avec un nom et surnom donne
	 * @param nom
	 * @param surnom
	 */
	public Alias(String nom, String surnom) {
		this.nom = nom;
		this.surnom = surnom;
	}
	
	/**
	 * Renvoie le nom de l'alias
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Renvoie le surnom de l'alias
	 * @return
	 */
	public String getSurnom() {
		return surnom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setSurnom(String surnom) {
		this.surnom = surnom;
	}
	
	/**
	 * Renvoie une r�presentation textuelle de l'alias
	 */
	public String toString() {
		return "Alias[ nom : "+nom+", surnom : " + surnom +"]";
	}

}
