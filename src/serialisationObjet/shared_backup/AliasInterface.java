package serialisationObjet.shared_backup;

import java.io.Serializable;

public interface AliasInterface extends Serializable {
	
	public abstract String getNom();
	
	public abstract String getSurnom();
	
	public abstract void setNom(String nom);
	
	public abstract void setSurnom(String surnom);

}
