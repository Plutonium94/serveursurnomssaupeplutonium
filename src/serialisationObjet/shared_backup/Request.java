package serialisationObjet.shared_backup;

/**
 * Un Request (requete) est un Message que le client envoie au serveur.
 * Il contient un code et un alias. Il exprime le souhaite du client que
 * la requete correpondate au code soit execute avec l'alias comme argument.
 * @author Plutonium94, Alain Saupe
 *
 */
public class Request extends Message {
	
	private Alias alias;
	
	public Request() {
		this(CodeRequest.LIST_ALL_NAMES_AND_NICKNAMES);
	}

	/**
	 * Construit un Request avec le code
	 * donn� et un alias null
	 * @param code
	 */
	public Request(CodeRequest code) {
		this(code,new Alias(null,null));
	}
	
	/**
	 * Construit un Request avec le code donn�
	 * et l'alias donn�e 
	 * @param code
	 * @param alias
	 */
	public Request(CodeRequest code, Alias alias)
	{
		super(code);
		this.alias = alias;
	}
	
	/**
	 * Renvoie l'alias de Request
	 * @return
	 */
	public Alias getAlias() {
		return alias;
	}
	
	/**
	 * Renvoie une representation textuelle de Request
	 */
	public String toString() {
		return "Request [ codeRequest : " + code + ", alias : " + alias + "]";
	}

}
